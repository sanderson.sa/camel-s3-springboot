# camel-s3-springboot

Rotear arquivos a partir de qualquer origem e enviar para a AWS S3 Service

# Testar
mvn clean package<br />
mvn spring-boot:run

# Atualizar credenciais AWS
src/main/resources/application.yml
